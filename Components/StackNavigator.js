import { StackNavigator} from 'react-navigation';

import Home from './MainTorchComponent';
import ScreenLightComponent from './ScreenLightComponent';

 export default StackNavigator({
    HomeScreen: { screen: Home },
    BrightnessScreen: { screen: ScreenLightComponent },
  }, {
    headerMode: 'none',

  });

 
  