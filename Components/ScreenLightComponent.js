import React, { Component } from 'react';
import Orientation from 'react-native-orientation-locker';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,Image,TouchableOpacity,Slider
} from 'react-native';
import store from 'react-native-simple-store';
import DeviceBrightness from 'react-native-device-brightness';


class ScreenLightComponent extends Component{
  constructor(props) {
    super(props);

    this.state = {
      value: 0.3,
    };
  }


  setBrightnessThumbValue(){
    store.get('thumbValue')
    .then((res) =>{
      if(res.value || res.value===0)
      {
        //console.log('updating state to :',res.value);
        this.setState({ 
          value:res.value
        },function(){
          DeviceBrightness.setBrightnessLevel(this.state.value);
        });
        
      }else{
       // console.log('Sorry! couldnt update the state');
      }
    });
  }

  componentDidMount(){

    clearInterval(this.timerInterval);
    Orientation.lockToPortrait();

  }

  componentWillMount(){
    this.setBrightnessThumbValue();
  }

  componentWillUnmount(){
    this.storeBrightnessThumbValue(); 
  }

  storeBrightnessThumbValue(){
    

    store.save('thumbValue', {
      'value': this.state.value,
    })
    .then(() => store.get('thumbValue'))
    .then((res) => {
       // console.log('UnMounting: Storing value:',res.value)
    });
  }

    _handleBrightness(value) {
      
      this.setState(() => {
        return {
          value: parseFloat(value),
        };
      });
      DeviceBrightness.setBrightnessLevel(value);
    }

    navigateToMainScreen =() =>{

      this.storeBrightnessThumbValue();
      this.props.navigation.navigate('HomeScreen');
  }

    render() {

    const { navigate } = this.props.navigation;
    const {value} = this.state;
    return (
        <View>
           <View style={styles.sliderContainer}>
          

                          <View style={{flex:1 , justifyContent:'flex-end', alignSelf:'flex-start'}}>
                              <Slider
                                    vertical= {true}
                                    step={0.1}
                                    style={styles.slider}
                                    minimumValue={0}
                                    maximumValue={1}
                                    onValueChange={this._handleBrightness.bind(this)}
                                    value={value}
                                    thumbTintColor='lightgreen'
                                    minimumTrackTintColor='gray'
                                    maximumTrackTintColor='white'
                                    
                                  />  

                          </View >

                      <View style={{flex:1 , justifyContent:'flex-end', alignSelf:'flex-start'}}>
                            <TouchableOpacity 
                              activeOpacity = { .5 }
                              onPress={() =>this.navigateToMainScreen() }>
                            <Image source={require('../images/button-flashlight-main.png')} />
                            </TouchableOpacity>
                      </View>

         </View>

                 
              
                
        </View>

    );
}

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2f2f2f',
  },slider:{
    alignSelf:'center',
    width: 360,
    height:20,
    backgroundColor: '#DCDCDC',
  transform: [
  { rotateZ : '-90deg' },
    ],
},
headline: {
    fontSize: 22,
    color: '#666'
},
sliderContainer: {
  height:616,

},


});

export default ScreenLightComponent;