import React, { Component } from 'react';
import Orientation from 'react-native-orientation-locker';
import {
  StyleSheet,
  View, Image, TouchableOpacity, ScrollView,TouchableWithoutFeedback,Dimensions
} from 'react-native';

import resolveAssetSource from 'resolveAssetSource';
import Torch from 'react-native-torch';
import DeviceBrightness from 'react-native-device-brightness';

const {height, width} = Dimensions.get('window');

class MainTorchComponent extends Component {

  constructor(props) {
        super(props);

        this.state = {
          isTorchOn: false,
          isMainButtonOn:false,
          selectedStrobeTimer:0,
          activeStorobe0:true,
          activeStorobeSOS:false,
          activeStorobe1:false,
          activeStorobe2:false,
          activeStorobe3:false,
          activeStorobe4:false,
          };

       
    }


  componentDidMount(){
    Orientation.lockToPortrait(); 

    Torch.switchState(false);
    this.clearTimeIntervals();
  }

  handleTimerInterval(){
  
    if(this.state.selectedStrobeTimer===0){
      this.clearTimeIntervals();
    }else{

        Torch.switchState(!this.state.isTorchOn);
        this.setState({ 
          isTorchOn:!this.state.isTorchOn,
          isMainButtonOn: true },function(){
            this.clearTimeIntervals();
            this.timerInterval=setInterval(this.handleTimerInterval.bind(this),this.state.selectedStrobeTimer);
          }); 
    }
}

  makeTorchOff(){
    const { isTorchOn ,isMainButtonOn} = this.state;
    Torch.switchState(false);
    this.setState({ 
      isTorchOn: false,
      isMainButtonOn:false
    });
  }

  makeTorchOn(){
    const { isTorchOn ,isMainButtonOn} = this.state;
    Torch.switchState(true);
    this.setState({ 
      isTorchOn: true,
      isMainButtonOn:true
    });
  }

  toggleTorch() {
  
    if(this.state.isMainButtonOn && !this.state.isTorchOn){

      this.clearTimeIntervals();
      this.makeTorchOff();

    }else if(!this.state.isTorchOn){

          if(this.timerInterval){
              this.clearTimeIntervals();
          }

          if(this.state.selectedStrobeTimer >0){
            this.handleTorchBlink();
          }else{
            this.makeTorchOn();
          }
        }else{
          
          this.clearTimeIntervals();
          this.makeTorchOff();
        }
  }

  scrollToEnd(){
    this.scrollView.scrollTo({x: 750,animation:true});
    this.setState({ selectedStrobeTimer:4000  },function(){
      this.updateSelectedStrobe(4);
      if(this.state.isTorchOn || this.state.isMainButtonOn){     
       
        this.handleTorchBlink();
      }

    });
  }

  handleScrollToStart (){
 
    this.scrollView.scrollTo({x: 0,animation:true});  
    this.setState({ 
        selectedStrobeTimer:0
     });

    this.clearTimeIntervals();
    this.updateSelectedStrobe(0);
    
  }

  clearTimeIntervals(){
    clearInterval(this.timerInterval);
  }

  deactiveAllStrobes(){
    this.setState({
      activeStorobe0:false,
      activeStorobeSOS:false,
      activeStorobe1:false,
      activeStorobe2:false,
      activeStorobe3:false,
      activeStorobe4:false,
    })
  }

  updateSelectedStrobe(activeStrobe){

    switch(activeStrobe){
      case 0:
        this.deactiveAllStrobes();
        this.setState({
          activeStorobe0:true
        });
        break;

        case 'SOS':
        this.deactiveAllStrobes();
        this.setState({
          activeStorobeSOS:true
        });
        break;
        
        case 1:
        this.deactiveAllStrobes();
        this.setState({
          activeStorobe1:true
        });
        break;

        case 2:
        this.deactiveAllStrobes();
        this.setState({
          activeStorobe2:true
        });
        break;

        case 3:
        this.deactiveAllStrobes();
        this.setState({
          activeStorobe3:true
        });
        break;

        case 4:
        this.deactiveAllStrobes();
        this.setState({
          activeStorobe4:true
        });
        break;

        default :
        this.deactiveAllStrobes();
        this.setState({
          activeStorobe0:true
        });
        
    }

    // this.setState({
    //   activeStrobeData:{
    //       activeStorobe0:true,
    //       activeStorobeSOS:false,
    //       activeStorobe1:false,
    //       activeStorobe2:false,
    //       activeStorobe3:false,
    //       activeStorobe4:false,
    //   }
    // });

  }
  

  checkScrollSituationAtStart(){

          if(this.state.selectedStrobeTimer===0){
            this.scrollToEnd();
          }else{
                this.setState({ selectedStrobeTimer:0  },function(){
                    this.clearTimeIntervals();
                    this.updateSelectedStrobe(0); 
                  //  if(this.state.isTorchOn  || this.state.isMainButtonOn){
                           
                  //  }
                });
                
           }
  }

  checkScrollSituationAtEnd(){

    if(this.state.selectedStrobeTimer===4000){
        this.handleScrollToStart();
    }else{

      this.setState({ selectedStrobeTimer:4000  },function(){
            this.updateSelectedStrobe(4);
            if(this.state.isTorchOn || this.state.isMainButtonOn){
              
              this.handleTorchBlink();
            }
          });
    }
}

  handleTorchBlink(){

    Torch.switchState(false);
    this.setState({
      isTorchOn:false,
      isMainButtonOn:false
    },function(){
        this.handleTimerInterval();
    })
  }

  onStrobeScroll = (e) =>{

    let contentOffset = e.nativeEvent.contentOffset;

    if(contentOffset.x<150){

        this.checkScrollSituationAtStart();

    }
    else if(contentOffset.x>=150 && contentOffset.x<300){

      this.setState({
        selectedStrobeTimer:500
      },function(){
        this.updateSelectedStrobe('SOS');
        if(this.state.isTorchOn || this.state.isMainButtonOn){
            
            this.handleTorchBlink();
        }
      });
    }else if(contentOffset.x>=300 && contentOffset.x<450){
      this.handleScrollSituation(1);

    }else if(contentOffset.x>=450 && contentOffset.x<600){ 
      this.handleScrollSituation(2);
    }else if(contentOffset.x>=600 && contentOffset.x<750){
        this.handleScrollSituation(3);
    }else{
      
      this.checkScrollSituationAtEnd();
    }
}

handleScrollSituation(timer){
  let time=timer*1000;
  this.setState({
    selectedStrobeTimer:time
  },function(){
    this.updateSelectedStrobe(timer);
    if(this.state.isTorchOn || this.state.isMainButtonOn){
        this.handleTorchBlink();
    }
  });
}

navigateToNextScreen =() =>{
    this.setState({
      selectedStrobeTimer:0
    });
    this.clearTimeIntervals();
    this.props.navigation.navigate('BrightnessScreen');
}

renderTorchTopImage(){

  if(this.state.isTorchOn){

    return (
        <View style={styles.topImageContainer}>
        <Image
            style={styles.topImage}
            source={require('../images/background-on1.png')}
            />
    </View>);
  } 
               
    return (
            <View style={styles.topImageContainer}>
                  <Image
                      style={styles.topImage}
                      source={require('../images/background-off1.png')}
                      />
              </View>
    )
                

}

renderTorchHandleImage(){

  if(this.state.isMainButtonOn){
      return (<View style={{flex:5,flexDirection :'row'  }}>

              <View style={{alignSelf:'flex-end',justifyContent:'flex-end' }} >
                <TouchableOpacity
                      activeOpacity = { .5 }
                      onPress={() =>this.navigateToNextScreen() }>
                      <Image source={require('../images/button-screenlight.png')} />
              </TouchableOpacity>

              </View>

              <View style={{marginLeft:15 }}>


            <View style={styles.upperHandleContainer}>
                  

                <TouchableWithoutFeedback
                  activeOpacity = { .5 }
                  onPressIn={this.toggleTorch.bind(this)}>
            
                      <Image
                          style={{width:150}}
                          source={require('../images/main-on.png')}
                        />
                </TouchableWithoutFeedback>

            </View>

              
            <View style={styles.lowerHandleContainer}>
            <Image
                    style={{width:150}}
                    source={require('../images/black-handle.png')}
                    />
            </View>
              
          </View>
          </View>);
  }  
  
  return (<View style={{flex:5,flexDirection :'row'  }}>

  <View style={{alignSelf:'flex-end',justifyContent:'flex-end' }} >
     <TouchableOpacity
          activeOpacity = { .5 }
          onPress={() =>this.navigateToNextScreen()}>
           <Image source={require('../images/button-screenlight.png')} />
   </TouchableOpacity>
  
  </View>
  
  <View style={{marginLeft:15 }}>
  
  
     <View style={styles.upperHandleContainer}>
         
         <TouchableWithoutFeedback
          activeOpacity = { .5 }
          onPressIn={this.toggleTorch.bind(this)}>
     
              <Image
                   style={{width:150}}
                   source={require('../images/main-off.png')}
                />
         </TouchableWithoutFeedback>
  
     </View>
  
       
     <View style={styles.lowerHandleContainer}>
     <Image
             style={{width:150}}
             source={require('../images/black-handle.png')}          
          />
     </View>
      
  </View>
  </View>);

}

renderScrollView(){

  let strobeIcon0 = this.state.activeStorobe0
  ? require('../images/0S.png')
  : require('../images/0S.png');
  let strobeIconSOS = this.state.activeStorobeSOS
  ? require('../images/SOSS.png')
  : require('../images/SOSS.png');
  let strobeIcon1 = this.state.activeStorobe1
  ? require('../images/1S.png')
  : require('../images/1S.png');
  let strobeIcon2 =  this.state.activeStorobe2
  ? require('../images/2S.png')
  : require('../images/2S.png');
  let strobeIcon3 =  this.state.activeStorobe3
  ? require('../images/3S.png')
  : require('../images/3S.png');
  let strobeIcon4 =  this.state.activeStorobe4
  ? require('../images/4S.png')
  : require('../images/4S.png');

  return (
    <View style={styles.speedScrollBarContainer}>
  <ScrollView 
      ref={(scrollView) => { this.scrollView = scrollView; }}
    
      pagingEnabled={true}
      horizontal= {true}
      decelerationRate={0}
      snapToInterval={150}
      snapToAlignment={"center"}
      onMomentumScrollEnd={(e) => this.onStrobeScroll(e)}
      >

          <View style={styles.scrollBarViews} >
     
          <Image
                  ref='strobeIcon0'
                    style={styles.smallScrollButtons}
                    source={strobeIcon0} /> 
          </View>
          <View style={styles.scrollBarViews} > 
          <Image
                    ref='strobeIconSOS'
                    style={styles.smallScrollButtons}
                    source={strobeIconSOS} />
          </View>
           
          
         <View style={styles.scrollBarViews}>
         <Image
                    ref='strobeIcon1'
                    style={styles.smallScrollButtons}
                    source={strobeIcon1} /> 
         </View>
          <View style={styles.scrollBarViews} >
          <Image
                    ref='strobeIcon2'
                    style={styles.smallScrollButtons}
                    source={strobeIcon2} /> 
          </View>
          <View style={styles.scrollBarViews}>
          <Image

                    ref='strobeIcon3'
                    style={styles.smallScrollButtons}
                    source={strobeIcon3} /> 
          </View> 
          <View style={styles.scrollBarViews}>
          <Image
                    ref='strobeIcon4'
                    style={styles.smallScrollButtons}
                    source={strobeIcon4} /> 
          </View> 
 
  </ScrollView>
  </View>
  );
}
  render() {

    return (
        <View style={styles.container}>

          {this.renderTorchTopImage()}
          {this.renderScrollView()}
          {this.renderTorchHandleImage()}
 
        </View>

    );
}

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2f2f2f',
  
  },
  topImageContainer:{
    width:360,
    height:277,
    alignItems: 'center',
  },
  topImage:{
    width:190,
    height:277,
  },
  speedScrollBarContainer:{
    height:35,  
    marginTop:0,
    marginBottom:0,
    width:150,
    alignSelf:'center',
    alignItems: 'center',
    justifyContent:'center',
  },torchHandle:{
  
    //width:150,
  },
  scrollBarViews: {
    backgroundColor: '#2F2F2F',
    width: 150,
    height:35,
    justifyContent: 'center',
    alignItems: 'center',
    borderRightColor:'red',

  },upperHandleContainer:{
    width:150,
    flexDirection:'row',
    justifyContent:'flex-start',
    alignItems:'flex-start',

  },lowerHandleContainer:{
    width:150,
  }

});

export default MainTorchComponent;

      