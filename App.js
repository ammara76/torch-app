import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
} from 'react-native';
import StackNavigator from './Components/StackNavigator';

class App extends Component {
  constructor(props) {
    super(props);
  }

  render(){
    return (
        <StackNavigator />
    );
}

}
export default App;